provider "aws" {
  region = "ap-south-1"
}

#--------------
# Create a VPC
#--------------

resource "aws_vpc" "my_vpc" {
  cidr_block       = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "venu VPC"
  }
}

# --------------------------------------------------------
# LIST OF AVAILABILITY ZONES IN THE CURRENT REGION
# --------------------------------------------------------

data "aws_availability_zones" "all" {}


#-------------------------------------------------
# Create a Public subnet on the First available AZ
#-------------------------------------------------

resource "aws_subnet" "pub_sub" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.subnet1_cidr
  availability_zone = data.aws_availability_zones.all.names[0]

  tags = {
    Name = "Public Subnet"
  }
}


#-------------------------------
# Create an IGW for VPC
#-------------------------------
resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "myIGW"
  }
}

#----------------------------------
# Create an RouteTable for Pub_Subnet
#----------------------------------
resource "aws_route_table" "public_RT" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.my_igw.id
    }

    tags = {
        Name = "Public RouteTable"
    }
}

#--------------------------------------------------------------
# Associate the Pub_RouteTable to the Pub_Subnet
#--------------------------------------------------------------
resource "aws_route_table_association" "pub_RT_association" {
    subnet_id = aws_subnet.pub_sub.id
    route_table_id = aws_route_table.public_RT.id
}


#---------------------------------------------------
# Create a Private subnet on the Second available AZ
#---------------------------------------------------
resource "aws_subnet" "private_sub" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.subnet2_cidr
  availability_zone = data.aws_availability_zones.all.names[1]

  tags = {
    Name = "Private Subnet"
  }
}

#--------------------------------------
#create NAT_gateway
#--------------------------------------------
resource "aws_eip" "nat_gateway" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_gateway.id
  subnet_id = aws_subnet.pub_sub.id
  tags = {
    "Name" = "myNatGateway"
  }
}


#--------------------------------------------------
# Create an RouteTable for private_subnet
#--------------------------------------------------
resource "aws_route_table" "private_RT" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.nat_gateway.id
    }
    tags = {
        Name = "my_Private RouteTable"
    }
}

#----------------------------------------------------------------
# Associate the private_RouteTable to the Private_Subnet
#----------------------------------------------------------------
resource "aws_route_table_association" "private_RT_association" {
    subnet_id = aws_subnet.private_sub.id
    route_table_id = aws_route_table.private_RT.id
}

#----------------------------------------------------------
# CREATE THE SECURITY GROUP THAT'S APPLIED To Virtual Machine EC2
#----------------------------------------------------------
resource "aws_security_group" "VM" {
  name = "virtual-server"
  vpc_id = aws_vpc.my_vpc.id

  # Allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Inbound for SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#------------------------------------
# Create EC2 virtual Machine on Private_Sub
#-------------------------------------
resource "aws_instance" "VM" {
   ami           = var.amiid
   instance_type = var.type
   key_name      = var.pemfile
   vpc_security_group_ids = [aws_security_group.VM.id]
   subnet_id = aws_subnet.private_sub.id
   availability_zone = data.aws_availability_zones.all.names[1]

   associate_public_ip_address = true

   tags = {
       Name = "Private_Server"
   }

}
